package com.project.ralph.appbottomnavigation;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.animation.Animation;

/**
 * Created by Ralph Orden on 10/27/2017.
 */

public class BaseFragment extends Fragment {

    public static <T extends BaseFragment> T newInstance(Class<T> clazz, Bundle bundle) {
        try {
            T newInstance = clazz.newInstance();
            newInstance.setArguments(bundle);
            return newInstance;
        } catch (java.lang.InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

//    @Override
//    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
//        Animation animation = new Animation() { };
//        animation.setDuration(0);
//        return animation;
//    }
}
