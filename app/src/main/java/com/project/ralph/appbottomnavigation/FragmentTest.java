package com.project.ralph.appbottomnavigation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Ralph Orden on 10/26/2017.
 */

public class FragmentTest extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_test, container, false);

        setRetainInstance(true);

        TextView textView = view.findViewById(R.id.tvTest);
        Button button = view.findViewById(R.id.bHome);

        Bundle arguments = getArguments();

        if (savedInstanceState == null) {
            if (arguments != null) {
                String label = arguments.getString("test");
                textView.setText(label);

                Toast.makeText(getContext(), label, Toast.LENGTH_SHORT).show();
            }
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.goToHome();
            }
        });

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
