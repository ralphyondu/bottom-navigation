package com.project.ralph.appbottomnavigation;

import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import static com.project.ralph.appbottomnavigation.BaseFragment.newInstance;

public class MainActivity extends AppCompatActivity {

    String lastAddedStackName;
    BottomNavigationViewEx bottomNavigationViewEx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationViewEx  = findViewById(R.id.bottomNavigationView);

        bottomNavigationViewEx.enableAnimation(false);
        bottomNavigationViewEx.enableShiftingMode(false);
        bottomNavigationViewEx.enableItemShiftingMode(false);

        checkLocationStatus();

        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                String label = item.getTitle().toString();

                Bundle bundle = new Bundle();
                bundle.putString("test", label);

                String name = "FragmentTest" + label;

                int order = item.getOrder();
                int current = bottomNavigationViewEx.getCurrentItem();

                BottomNavigationItemView bottomNavigationItemView = bottomNavigationViewEx.getBottomNavigationItemView(order);
                bottomNavigationItemView.setTag(name);

                if (order != current) {
                    BottomNavigationItemView bottomNavigationItemViewCurrent = bottomNavigationViewEx.getBottomNavigationItemView(current);

                    if (bottomNavigationItemViewCurrent != null) {
                        if (isHomeVisible()) {
                            lastAddedStackName = HomeFragment.class.getSimpleName();
                        } else {
                            lastAddedStackName = (String) bottomNavigationItemViewCurrent.getTag();
                        }
                    }
                    updateTabColor(bottomNavigationItemViewCurrent, R.color.colorPrimary);
                } else {
                    if (isHomeVisible()) {
                        lastAddedStackName = HomeFragment.class.getSimpleName();
                    }
                }

                log();

                updateTabColor(bottomNavigationItemView, R.color.colorAccent);

                Fragment fragment = newInstance(FragmentTest.class, bundle);

                if (fragment != null) {
                    replaceFragment(getSupportFragmentManager(), fragment, name, lastAddedStackName);
                }

                return true;
            }
        });

        bottomNavigationViewEx.setSelectedItemId(R.id.menuOne);
    }

    private void log() {
        Log.d("MainActivity", "log: " + lastAddedStackName);
    }

    public void goToHome() {
        String name = HomeFragment.class.getSimpleName();

        int current = bottomNavigationViewEx.getCurrentItem();
        BottomNavigationItemView bottomNavigationItemViewCurrent = bottomNavigationViewEx.getBottomNavigationItemView(current);

        if (bottomNavigationItemViewCurrent != null) {
            updateTabColor(bottomNavigationItemViewCurrent, R.color.colorPrimary);

            bottomNavigationViewEx.setItemBackground(current, R.color.colorPrimary);
            lastAddedStackName = (String) bottomNavigationItemViewCurrent.getTag();

            log();
        }

        replaceFragment(getSupportFragmentManager(), newInstance(HomeFragment.class, null), name, lastAddedStackName);
    }

    private boolean isHomeVisible() {
        Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(HomeFragment.class.getSimpleName());
        return fragmentByTag != null && fragmentByTag.isVisible();
    }

    private void updateTabColor(BottomNavigationItemView bottomNavigationItemView, int color) {
        if (bottomNavigationItemView == null);
        bottomNavigationItemView.setBackgroundColor(ContextCompat.getColor(this, color));
    }

    public String replaceFragment(FragmentManager fm, Fragment fragment, String backStackName, String lastAddedStackName) {
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        if (fm.findFragmentByTag(backStackName) != null) {
            fragmentTransaction
                    .setTransition(FragmentTransaction.TRANSIT_NONE)
                    .show(fm.findFragmentByTag(backStackName)).commit();
        } else {
            fragmentTransaction
                    .setTransition(FragmentTransaction.TRANSIT_NONE)
                    .add(R.id.containerFrame, fragment, backStackName).commit();
        }

        if (fm.findFragmentByTag(lastAddedStackName) != null) {
            fm.beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_NONE)
                    .hide(fm.findFragmentByTag(lastAddedStackName)).commit();
        }

        return backStackName;
    }

    private void checkLocationStatus() {

    }
}
